# Introduction

Le projet *premier-samedi* est un site web pour la promotion et la gestion d'une des activités de l'assocation Parinux (http://www.parinux.org/).

À l'origine, le site était basé Wordpress et depuis février 2016, le site est basé sur SPIP. Ce projet porte cette version.

# Organisation des fichiers

L'arborescence du projet est la suivante :

* nginx : contient le fichier de configuration de notre serveur nginx qui fait proxy vers notre conteneur LXC,
* apache2 : contient le fichier de configuration du serveur apache2 dans le LXC,
* spip : contient les fichiers de configuration de spip ;
* sql : content un export de la base de données utilisée par SPIP avec notamment les articles associés aux bons numéro d'identifiants.

# Déploiement sous GNU/Linux Debian

Mettre à jour le système :
```
% apt-get update && apt-get upgrade
```

Installer le moteur du site :
```
% apt-get install apache2 spip php5 php5-gd imagemagick
```

Déclarer un nouveau site dans SPIP :
```
% spip_add_site premier-samedi.parinux.org

% cd /etc/apache2/sites-available
% ln -s /srv/premier-samedi/git/apache2/premier-samedi.parinux.org
% systemctl restart apache2
```

Créer un dossier projet et y cloner le projet git (nous supposons que vous avez un compte Framagit) :
```
% mkdir -p /services/premier-samedi/
% cd /services/premier-samedi/
% git clone git@framagit.org:Parinux/premier-samedi.git
% mv premier-samedi git
```

Brancher les fichiers du projet sur l'environnement SPIP : (**en cours de rédaction**)
```
% cd /etc/spip/sites-files/premier-samedi.parinux.org/
% ln -s /srv/premier-samedi/git/spip/squelettes

% cd /etc/spip/sites-files/premier-samedi.parinux.org/config/
% ln -s /srv/premier-samedi/git/spip/config/mes_options.php

% cd /var/lib/spip/
% ln -s /srv/premier-samedi/git/spip/.htaccess
```

Importer les articles :
```
% cd /services/premier-samedi/git/sql/
% sudo mysql -u user -p databasename < premier-samedi.sql
```

**À compléter…**