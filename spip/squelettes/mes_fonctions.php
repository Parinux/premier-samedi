<?php

function filtre_psl_datelitterale ($texte) {
	return strftime( "%e %B %Y", $texte);
}

function filtre_psl_date_iso ($texte) {
	return strftime("%Y-%m-%d", $texte);
}


/**
* Filtre sur le titre du site supprimant le protocole (http ou https) pour ne pas l'écrire « en dur »
*/
function filtre_psl_supprimeprotocole($texte) {
	$idx = strpos($texte, "//");
	if ($idx > 0) {
		return substr($texte, $idx);
	} else {
		return $texte;
	}
	
}

/***********************
* Balises propres au site
* Elles commencent toutes par PSL_
***********************/

function balise_PSL_PROCHAINEDATE($p) {
	$p->code = "psl_balise_ProchaineDate()";
return $p;
}

function balise_PSL_EXPOSE($p) {
  $articleEnCours = "''";
    $articlePrincipal = "''";
    $rubriquePrincipale = "''";
    if (($v = interprete_argument_balise(1, $p)) !== null) {
	    $articleEnCours = $v;
	    if (($v = interprete_argument_balise(2, $p)) !== null) {
		    $articlePrincipal = $v;
		    if (($v = interprete_argument_balise(3, $p)) !== null) {
		      $rubriquePrincipale = $v;
	      }
	    }

    }
  $p->code = "psl_balise_Expose($articleEnCours, $articlePrincipal, $rubriquePrincipale)";
  return $p;
}

/***********************
* Fonctions de calcul des balises
***********************/

function psl_balise_Expose ($articleEnCours, $articlePrincipal, $rubriquePrincipale) {
  $on = false;
  if ($articleEnCours == $articlePrincipal) {
    $on = true;
  } else if (($articleEnCours == 3) && ($rubriquePrincipale == 5)) {
    $on = true;
  } else if (($articleEnCours == 2) && ($rubriquePrincipale == 4)) {
    $on = true;
  }
  if ($on) {
    return 'on';
  } else {
    return '';
  }
}

function psl_balise_ProchaineDate () {
  $_debutMoisCourrant = mktime(0,0,0, date("n"), 0, date("Y"));
  $_debutMoisProchain = mktime(0,0,0, date("n")+1, 0, date("Y"));
  $_debutMoisSuivant = mktime(0,0,0, date("n")+2, 0, date("Y"));

  $_premierSamediMoisCourant = strtotime('first saturday',$_debutMoisCourrant);
  $_premierSamediMoisProchain = strtotime( 'first saturday',$_debutMoisProchain);
  $_premierSamediMoisSuivant = strtotime( 'first saturday',$_debutMoisSuivant);

  $_dateJour = mktime(0,0,0, date("n"), date( "d"), date("Y"));

  setlocale(LC_TIME, 'fr_FR.UTF8');
  if ( $_dateJour > $_premierSamediMoisCourant ) {
    return $_premierSamediMoisProchain;
  } else {
    return $_premierSamediMoisCourant; 
  }
}
